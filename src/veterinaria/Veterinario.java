package veterinaria;

import java.util.ArrayList;

public class Veterinario extends Persona{
    
    private ArrayList<String> animales;
    private int[] dias;

    public Veterinario(ArrayList<String> animales, int[] dias, String usuario, String password) {
        super(usuario, password);
        this.animales = animales;
        this.dias = dias;
        setCargo("VETERINARIO");
    }
    private StockProducto pro;
    @Override
    public boolean proceder() {
        boolean cerrar = false;
        while (!cerrar) {
            int op;
            do {
                op = EntradaSalida.leerInt(
                        "MENÚ VETERINARIO \n\n                                                                                                                                                                              \n\n"
                        + "[0] Cerrar Sesión\n"
                        + "[1]-Vender producto"
                                + "\n");
            } while (op < 0 || op > 1);

           if(op==0){
                    EntradaSalida.mostrarString("Hasta mañana!");
                    cerrar=true;
           }else{
                    pro.venderProducto(0);
            }
        }
        return true;
    }

    public ArrayList<String> getAnimales() {
        return animales;
    }

    public StockProducto getPro() {
        return pro;
    }

    public void setPro(StockProducto pro) {
        this.pro = pro;
    }

    public int[] getDias() {
        return dias;
    }
    public String mostrar(){
        String ver="";
        for(int i=0;i<animales.size();){
            ver+=animales.get(i)+" -";
        }
        return "\nVeterinario "+getUsuario() + ":" + ver;
    }
    public void setDias(int[] dias) {
        this.dias = dias;
    }
    
}
