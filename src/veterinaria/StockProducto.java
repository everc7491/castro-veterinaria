package veterinaria;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class StockProducto implements Serializable{
    private List<Producto> productos;

    public List<Producto> getProductos() {
        return productos;
    }

    public void setProductos(List<Producto> productos) {
        this.productos = productos;
    }
     public StockProducto(){
         productos= new ArrayList<>();
     }

    void altaMedicamento(String nomProd, float precio,int cant) {
       Medicamento m = new Medicamento(nomProd,precio,cant);
       productos.add(m);
    }

    void altaRegular(String nomProd, float precio,int cant) {
        Regular r=new Regular(nomProd,precio,cant);
        productos.add(r);
    }

    String obtenerRegulares() {
          String lista = "[0]-CANCELAR";
       Producto p=null;
        if(productos == null){     
             EntradaSalida.mostrarString("No hay Productos Regulares cargados");
                } else{
                  for (int i = 0; i < productos.size(); i++) {
                   if(productos.get(i).getTipoProd().equals("REGULAR")){
                      p=productos.get(i);              
                      lista +="\n["+(i+1)+"]"+p.getDescripcion() + "$" +p.getPrecio();  
                   }
                        }
        }
        return lista;
    }

    void venderProducto(int i) {
      int j,cant;
      
          boolean existeCod=false;
           ArrayList<Integer> n=new ArrayList<>();
      
           do{
               if(i == 1){
               j=EntradaSalida.leerInt("Lista de Productos:\n\n"
                   + obtenerRegulares());
                n=new ArrayList<>(obtenerCodDisp(true));
                j--;
               for(int z : n){
                  if( z == j || j == -1){
                     existeCod=true;
                     break;
                      }
                  }      
        }else{
            j=EntradaSalida.leerInt("Lista de Productos:\n\n"
                    + obtenerProductos());
                 n=new ArrayList<>(obtenerCodDisp(false));
           j--;
           for(int z : n){
               if(z == j || j == -1){
                   existeCod=true;
                   break;
               }
        }
        }
        }while(!existeCod);
      if(j != -1){
          
           cant=EntradaSalida.leerInt("Cantidad a vender:");
           
            if(productos.get(j).getCantidad()-cant>=0){
                productos.get(j).setCantidad(productos.get(j).getCantidad()-cant);
                EntradaSalida.mostrarString("Venta realizada con exito:\n"
                        + "" + cant +productos.get(j).getDescripcion()
                        + "$" +(cant+productos.get(j).getPrecio()) );
             }else{
                 EntradaSalida.mostrarString("La cantidad solicitada supera el stock");
             }
       }else{
      EntradaSalida.mostrarString("Compra cancelada");
      }
    }
    private String obtenerProductos() {
              String lista = "[0]-CANCELAR";
       Producto p=null;
        if(productos==null){     
             EntradaSalida.mostrarString("No hay Productos cargados");
                } else{
                  for (int i = 0; i < productos.size(); i++) {
                      p=productos.get(i);              
                      lista +="\n["+(i+1)+"]"+productos.get(i).getDescripcion();
                        }
        }
        return lista;
    }
    ArrayList obtenerCodDisp(boolean r){   
    ArrayList<Integer> num=new ArrayList<Integer>();
    Producto pro=null;
        if(productos!=null){     
               for (int i = 0; i < productos.size(); i++) {
               pro=productos.get(i);
                   if((pro.getPrecio())!=0){
                    if(!r){
                        num.add(i);
                    }else if(pro.getTipoProd().equals("REGULAR")){
                        num.add(i);
                    }
                    }
                } 
        }
    return num;
    }
}
