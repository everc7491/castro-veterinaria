package veterinaria;

public abstract class Persona {
    private String usuario;
    private String password;
    private String cargo;

    public String getUsuario() {
        return usuario;
    }

    public String getPassword() {
        return password;
    }

    public Persona(String usuario, String password) {
        this.usuario = usuario;
        this.password = password;
    }
    public abstract boolean proceder();

    public String getCargo() {
        return cargo;
    }

    public void setCargo(String cargo) {
        this.cargo = cargo;
    }
    
}
