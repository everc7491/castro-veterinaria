package veterinaria;

public abstract class Producto {
    private String descripcion;
    private float precio;
    private int cantidad;
    private String tipoProd;
    
    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public float getPrecio() {
        return precio;
    }

    public void setPrecio(float precio) {
        this.precio = precio;
    }

    public Producto(String descripcion, float precio,int cantidad) {
        this.descripcion = descripcion;
        this.precio = precio;
        this.cantidad= cantidad;
    }

    public int getCantidad() {
        return cantidad;
    }

    public void setCantidad(int cantidad) {
        this.cantidad = cantidad;
    }

    public String getTipoProd() {
        return tipoProd;
    }

    public void setTipoProd(String tipoProd) {
        this.tipoProd = tipoProd;
    }
    
}
