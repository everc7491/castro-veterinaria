package veterinaria;

import java.io.IOException;

public class Main {

    public static void main(String[] args) {
   String archivoDatos = "veterinariaDatos.bin";
        Sistema s = new Sistema();

        try {
            s.serializar(archivoDatos);
        } catch (IOException ex) {
            EntradaSalida.mostrarString(ex.getMessage());
        }
        try {
            s = s.deSerializar(archivoDatos);
        } catch (IOException | ClassNotFoundException ex) {
            s.inicializacion();
        } finally {
            s.arrancar();
        }
    }
    
}

