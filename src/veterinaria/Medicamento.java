package veterinaria;

public class Medicamento extends Producto{
    
    public Medicamento(String descripcion, float precio,int cantidad) {
        super(descripcion, precio,cantidad);
        setTipoProd("MEDICAMENTO");
    }
    
}