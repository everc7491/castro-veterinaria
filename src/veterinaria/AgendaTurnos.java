package veterinaria;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class AgendaTurnos implements Serializable{
    private List<Turno> turnos;
  
     
    public AgendaTurnos(){
        turnos=new ArrayList<>();
       
    }

    

  
    void hayTurnos(Date dia,String v, Cliente c){
        Turno t= new Turno(dia, v),tu=null;
        boolean fechaEncontrada= false;
        int tur=0,i=0;
        
        if(turnos != null){
          for( i=0; i < turnos.size(); i++){
              tu=turnos.get(i);
               if((tu.getV().equals(v)) && tu.getDia().compareTo(dia)==0 ){
                   EntradaSalida.mostrarString("Dia fue previamente cargado");
                  do{
                       tur= EntradaSalida.leerInt(tu.mostrarDisponibles());
                  }while(!tu.horaCorrecta(tur));  
                  
               if(tur != 14){
                  tu.cargarHora(tur, c);
                  fechaEncontrada = true;
                  turnos.get(i).setC(tu.getC());
                  break;
               }
            }
          }
          if(!fechaEncontrada){
              do{
                   tur= EntradaSalida.leerInt(t.mostrarDisponibles());
                }while(!t.horaCorrecta(tur));
              if(tur != 14){
                   t.cargarHora(tur, c);
                   agregarTurno(t);
              }
          }
                             
        }else{
            EntradaSalida.mostrarString("Dia nuevo sin turnos cargados");
                do{
                   tur= EntradaSalida.leerInt(t.mostrarDisponibles());
                }while(!t.horaCorrecta(tur));
                if(tur != 14){
                   t.cargarHora(tur, c);
                   agregarTurno(t);
                }
            }
     
      
    }

    void agregarTurno(Turno t) {
       if(t!=null){
           turnos.add(t);
       }
    }

    public List<Turno> getTurnos() {
        return turnos;
    }

    public void setTurnos(List<Turno> turnos) {
        this.turnos = turnos;
    }

  


}

