package veterinaria;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class Dia implements Serializable{
    private List<Veterinario> lunes;
    private List<Veterinario> martes;
    private List<Veterinario> miercoles;
    private List<Veterinario> jueves;
    private List<Veterinario> viernes;   
    private List<Veterinario> sabado;
               
    public Dia(){
        lunes=new ArrayList<>();
        martes=new ArrayList<>();
        miercoles=new ArrayList<>();
        jueves=new ArrayList<>();
        viernes=new ArrayList<>();
        sabado=new ArrayList<>();
        
    }
    public void agregarLunes(Veterinario v){
        lunes.add(v);
    }
     public void agregarMartes(Veterinario v){
        lunes.add(v);
    }
      public void agregarMiercoles(Veterinario v){
        lunes.add(v);
    }
       public void agregarJueves(Veterinario v){
        lunes.add(v);
    }
        public void agregarViernes(Veterinario v){
        lunes.add(v);
    }
         public void agregarSabado(Veterinario v){
        lunes.add(v);
    }

    public List<Veterinario> getLunes() {
        return lunes;
    }

    public void setLunes(List<Veterinario> lunes) {
        this.lunes = lunes;
    }

    public List<Veterinario> getMartes() {
        return martes;
    }

    public void setMartes(List<Veterinario> martes) {
        this.martes = martes;
    }

    public List<Veterinario> getMiercoles() {
        return miercoles;
    }

    public void setMiercoles(List<Veterinario> miercoles) {
        this.miercoles = miercoles;
    }

    public List<Veterinario> getJueves() {
        return jueves;
    }

    public void setJueves(List<Veterinario> jueves) {
        this.jueves = jueves;
    }

    public List<Veterinario> getViernes() {
        return viernes;
    }

    public void setViernes(List<Veterinario> viernes) {
        this.viernes = viernes;
    }

    public List<Veterinario> getSabado() {
        return sabado;
    }

    public void setSabado(List<Veterinario> sabado) {
        this.sabado = sabado;
    }

    void especialidadesPordia() {
       String lista="",a="";
       
     //  Veterinario aux= null;
       if(lunes!=null){
           for ( Veterinario aux : lunes){
             lista+=aux.mostrar();
           }
           
       }else{
           lista+="-";   
       }
     
       EntradaSalida.mostrarString("LUNES\n"+lista);
       lista="MARTES\n";
        if(martes!=null){
           for (Veterinario aux : martes){
             lista+=aux.mostrar();
           }
           
       }else{
           lista+="------";   
       }
       EntradaSalida.mostrarString(lista);
            lista="MIERCOLES\n";
         if(miercoles!=null){
           for (Veterinario aux : miercoles){
             lista+=aux.mostrar();
           }
           
       }else{
           lista+="------";   
       }
        EntradaSalida.mostrarString(lista);
         lista="JUEVES\n";
          if(jueves!=null){
           for (Veterinario aux : jueves){
             lista += aux.mostrar();
           }
           
       }else{
           lista+="------";   
       }
          EntradaSalida.mostrarString(lista);
          
          lista="VIERNES\n";
           if(viernes!=null){
           for (Veterinario aux : viernes){
             lista+= aux.mostrar();
           }
           
       }else{
           lista += "------";   
       }
           EntradaSalida.mostrarString(lista);
           lista = "SABADO\n";
            if (sabado!=null){
           for (Veterinario aux : sabado){
             lista+=aux.mostrar();
           }
           
       }else{
           lista+="------";   
       }
            EntradaSalida.mostrarString(lista);
     
    }

    boolean hayEspecialidad(int day, String tipo) {
       boolean hayVeterinario=false;
       switch(day){
           case 1:
               hayVeterinario=verDia(tipo,lunes);
               break;
           case 2:
                hayVeterinario=verDia(tipo,martes);
               break;
           case 3:
                hayVeterinario=verDia(tipo,miercoles);
               break;
           case 4:
                hayVeterinario=verDia(tipo,jueves);
               break;
           case 5:
                hayVeterinario=verDia(tipo,viernes);
               break;
           case 6:
                hayVeterinario=verDia(tipo,sabado);
               break;
       }
       
       return hayVeterinario; 
    }

    private boolean verDia(String tipo, List<Veterinario> dia) {
       boolean hay=false;
       if(dia!=null){
         for(Veterinario v :dia){
              for(String a : v.getAnimales()){
                  if(a.equals(tipo)){
                      hay=true;
                      break;
                  }
              }
         }
       }
       return hay;
    }

    String elegirVeterinario(Date d, String tipoMascota) {
        String lista="", v="";
        
        switch(d.getDay()){
            case 1:
                  v=elegir(lunes,tipoMascota);
                  break;
            case 2:
                  v=elegir(martes,tipoMascota);
                  break;
            case 3:
                v=elegir(miercoles,tipoMascota);
                  break;
            case 4:
                v=elegir(jueves,tipoMascota);
                  break;
            case 5:
                v=elegir(viernes,tipoMascota);
                  break;
            case 6:
                v=elegir(sabado,tipoMascota);
                  break;
        }
        EntradaSalida.mostrarString("veterinario elegido, "+v);
        return v;
    }

    private String elegir(List<Veterinario> d,String tipoMascota) {
        String lista="-";
        String v ="";
        int i=0,op;
       if(d!=null){
           for (Veterinario vet : d){
             for(int j = 0 ;j < vet.getAnimales().size(); j++){
                 if(vet.getAnimales().get(j).equals(tipoMascota)){
         
                     lista+="["+i+"]"+vet.getUsuario()+"\n";
                    }
             }
             i++;
           }
            
       }else{
           lista+="-";   
       }
         if(lista.equals("-")){
             EntradaSalida.mostrarString("no hay disponible");
         }else{
         do{
             op=EntradaSalida.leerInt("Veterinarios disponibles para el dia:\n"+lista);
         }while(op<0||op>i);
        v = d.get(op).getUsuario();
        // v =seleccionarNombre(d,i);
  
         
                 }
         return v;
    }

    private String seleccionarNombre(List<Veterinario> d, int i) {
        String g="";
        int j=0;
        for(Veterinario v : d){
            if(i == j){
                g=v.getUsuario();
            }
            j++;
        }
        return g;
    }

    

   
                
    
}
