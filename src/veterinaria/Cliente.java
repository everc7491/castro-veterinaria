package veterinaria;

public class Cliente {
    private String nombre;
    private String duenio;
    private String telefono;
    private String tipoMascota;

    public Cliente(String nombre, String duenio, String telefono,String tipoMascota) {
        this.nombre = nombre;
        this.duenio = duenio;
        this.telefono = telefono;
        this.tipoMascota = tipoMascota;
    }

    public String getNombre() {
        return nombre;
    }

    public String getDueño() {
        return duenio;
    }

    public String getTelefono() {
        return telefono;
    }

    public String getTipoMascota() {
        return tipoMascota;
    }

    public void setTipoMascota(String tipoMascota) {
        this.tipoMascota = tipoMascota;
    }
    
}
