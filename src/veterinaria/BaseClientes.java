package veterinaria;


import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class BaseClientes implements Serializable{
 private List<Cliente> clientes;
 public BaseClientes(){
     clientes=new ArrayList<>();
 }

    public List<Cliente> getClientes() {
        return clientes;
    }

    boolean buscarCliente(String nombre, String dueño) {
       boolean clie=false;
       for(Cliente c: clientes){
           if(c.getNombre().equals(nombre)&&c.getDueño().equals(dueño)){
               clie=true;
           }
       }
       return clie;
    }

    Cliente obtenerCliente(String nombre, String dueño) {
      Cliente cl=null;
        for(Cliente c: clientes){
           if(c.getNombre().equals(nombre)&&c.getDueño().equals(dueño)){
               cl=c;
           }
         }
      return cl;
    }

   

    void agregarCliente(Cliente c) {
        if(c!=null){
            clientes.add(c);
        }
    }
}
