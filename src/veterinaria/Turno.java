package veterinaria;


import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class Turno {
    private Date dia;
    private String v;
    private Cliente[] c=new Cliente[14];

 

    public Turno(Date dia, String v) {
        this.dia = dia;
        this.v = v;
       for(int i=0;i<14;i++){
           c[i]=null;
       }
    }

    public Date getDia() {
        return dia;
    }

  
    String mostrarDisponibles() {
       String disp="Turnos disponibles: \n\n";
       Date d=new Date(0, 0, 0, 9, 0);
    
       for(int i=0;i<14;i++){
           if(c[i] != null){
               disp+="["+i+"]"+d.getHours()+":"+d.getMinutes()+"-OCUPADO\n"; 
           }else{
               disp+="["+i+"]"+d.getHours()+":"+d.getMinutes()+"-Disponible\n";
           }
          d.setMinutes(d.getMinutes()+30);
          if(i == 5){
              d.setHours(d.getHours()+1);
          }
       }
       disp+="\n [14]-Cancelar";
       
      return disp;
    }

String imprimirHora(Date d, Cliente[] c){
    String aux="";
         for(int i=0; i<14;i++){
                  if(c[i]==null){
                     
                  aux+="["+i+"]-"+d.getHours()+":"+d.getMinutes()+"\n";
                          }
                  d.setMinutes(d.getMinutes()+30);
              }
         return aux;
}

    public String getV() {
        return v;
    }

    public void setV(String v) {
        this.v = v;
    }

    public Cliente[] getC() {
        return c;
    }

    public void setC(Cliente[] c) {
        this.c = c;
    }

    void cargarHora(int tur, Cliente cl) {
       c[tur]=cl;
    }

    boolean horaCorrecta(int tur) {
           boolean disp=false;
            if(tur == 14){
           return
                   true;
       }
       for(int i=0;i<14;i++){
           if(c[tur]!=null){
               EntradaSalida.mostrarString("Turno ya asignado!!!");
               break;
           }else{
               disp=true;
           }
         
       }
      
      return disp;
    }

   
    
  

}
