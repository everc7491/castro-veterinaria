package veterinaria;

public class Regular extends Producto{
    
    public Regular(String descripcion, float precio,int cantidad) {
        super(descripcion, precio,cantidad);
        setTipoProd("REGULAR");
    }
    
}
