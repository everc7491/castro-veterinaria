package veterinaria;

import java.util.Date;

public class Recepcionista extends Persona{
    
    public Recepcionista(String usuario, String password) {
        super(usuario, password);
        setCargo("RECEPCIONISTA");
    }
    private StockProducto pro;
    private BaseClientes bCli;
    private Dia dias;
    private Date hoy;
    private AgendaTurnos agenda;
    @Override
    public boolean proceder() {
       boolean cerrar = false;
       String nombre="",duenio="";
      
       int i;
        while (!cerrar) {
            int op,dia,mes,anio;
            do {
                op = EntradaSalida.leerInt(
                        "MENÚ RECEPCIONISTA\n\n"
                        + "[0] Cerrar Sesión\n"
                        + "[1] Vender Productos\n"
                        + "[2]-Dar turno\n"
                        + "[3]-Ver Disponibilidad de Veterinarios");
            } while (op < 0 || op > 3);

            switch(op){ 
                case 0:
                cerrar = true;
                EntradaSalida.mostrarString("¡Hasta pronto!");
                break;
                
                case 1:
                    pro.venderProducto(1);
                    break;
                     
                case 2:
                    Cliente c=null;
                    nombre=EntradaSalida.leerString("Ingrese nombre de la mascota");
                    duenio=EntradaSalida.leerString("Ingrese nombre del dueño de "+nombre);
                    
                    if(bCli.buscarCliente(nombre,duenio)){
                         c=bCli.obtenerCliente(nombre,duenio);
                        
                    }else{
                        
                         c=altaCliente(nombre,duenio);
                    }            
                  
                       EntradaSalida.mostrarString("A continuacion Indicar fecha para el turno");
                     do{
                        dia=EntradaSalida.leerInt("DIA[dd]");
                    }while(dia<0||dia>31);
                    do{
                        mes=EntradaSalida.leerInt("MES[MM]");
                    }while(mes<0||mes>12);
                    do{
                        anio=EntradaSalida.leerInt("AÑO[yyyy]");
                    }while(anio<hoy.getYear());
                        Date d=new Date(anio-1900, mes-1, dia,0,0);
     
                        if(d.compareTo(hoy)<0||d.getDay()==0){
                             EntradaSalida.mostrarString("Fecha/Dia no valido");
                             }else{
                                  if(dias.hayEspecialidad(d.getDay(),c.getTipoMascota())){
                                      String ve=dias.elegirVeterinario(d,c.getTipoMascota());
                                
                                      agenda.hayTurnos(d,ve,c);
                            
                                     }else{
                                        EntradaSalida.mostrarString("No hay Veterinario disponible");
                       
                        }
                    }
                    break;
                        case 3: 
                          EntradaSalida.mostrarString("A continuacio Se listaran los veterinarios que hay cada dia con su respectiva especialidad");
               //AQUI DEBERIA MOSTRAR A LOS VETERINARIOS POR DIA Y ESPECIALIDAD          dias.especialidadesPordia();
                    }
                   
            
            }
        
        return true;
    }

    public StockProducto getPro() {
        return pro;
    }

    public void setPro(StockProducto pro) {
        this.pro = pro;
    }

    public BaseClientes getbCli() {
        return bCli;
    }

    public void setbCli(BaseClientes bCli) {
        this.bCli = bCli;
    }

    private Cliente altaCliente(String nombre, String dueño) {
        String tipo="",tel;
        int i;
        EntradaSalida.mostrarString("A continuacion daremos de alta al cliente");
                do{           
                        tel=EntradaSalida.leerString("Telefono de contacto");
                }while(noEsNumero(tel));
                         do{
                         i=EntradaSalida.leerInt("Tipo de mascota es "+nombre
                              +"\n\n[1]-Perro"
                                 + "\n[2]-Gato"
                                 + "\n[3]-Tortuga"
                                 + "\n[4]-Canario");
                            }while(i<1||i>4);
                         switch(i){
                         case 1:
                              tipo="PERRO";
                              break;
                         case 2:
                              tipo="GATO";
                                   break;
                         case 3:
                              tipo="TORTUGA";
                              break;
                         case 4:
                              tipo="CANARIO";
                              break;
                              }
                       Cliente c=new Cliente(nombre,dueño,tel,tipo);
                       bCli.agregarCliente(c);
           return c;
    }

    public Date getHoy() {
        return hoy;
    }

    public void setHoy(Date hoy) {
        this.hoy = hoy;
    }

   

    public AgendaTurnos getAgenda() {
        return agenda;
    }

    public void setAgenda(AgendaTurnos agenda) {
        this.agenda = agenda;
    }


    public Dia getDias() {
        return dias;
    }

    public void setDias(Dia dias) {
        this.dias = dias;
    }

    private boolean noEsNumero(String tel) {
        for(int i=0;i<tel.length();i++){
            
            if(tel.charAt(i)>'9'||tel.charAt(i)<'0'){
                 EntradaSalida.mostrarString("Ingrese un numero de telefono, sin guiones ni otros caracteres!");
                return true;
               
            }
        }
        return false;
    }

 
}
