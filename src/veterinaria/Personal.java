package veterinaria;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class Personal implements Serializable{
    private List<Persona> personas;

    public List<Persona> getPersonas() {
        return personas;
    }

    public void setPersonas(List<Persona> personas) {
        this.personas = personas;
    }
    public Personal(){
        personas=new ArrayList<>();
    }

    boolean verificarUsuario(String usu) {
     boolean verificacion=false;
     for (Persona p : personas) {
                if (p.getUsuario().equals(usu)) {
                    verificacion=true;
               
                }
            }
    return verificacion;
    }

    void agregarPersona(Persona p) {
       if(p!=null){
           personas.add(p);
       }
    }
String verPersonal(){
    String lis="--";
    if(personas!=null){
    for(Persona p : personas){
        lis+=p.getUsuario()+"-"+p.getPassword();
    }
    }
  return lis;
}
      Persona buscar(String usuario, String password) {
        Persona u=null;
        for (Persona p : personas) {
                if (p.getUsuario().equals(usuario) && p.getPassword().equals(password)) {
                    u = p;
               
                }
            }
        return u;
    }

    boolean hayVeterinario(int day) {
       boolean hay=false;
       for(Persona p: personas){
         if(p.getCargo().equals("VETERINARIO")){
             Veterinario v=(Veterinario)p;
          for (int i=0;i<3;i++){
              if(day==v.getDias()[i]){
                  hay=true;
              }
          }
       }
       }
       return hay;
    }
}
