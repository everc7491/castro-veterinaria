package veterinaria;

import java.util.ArrayList;
import java.util.Date;

public class Administrador extends Persona{
    
    public Administrador(String usuario, String password) {
        super(usuario, password);
        setCargo("ADMINISTRADOR");
    }
private StockProducto productos;
private Personal personas;
private BaseClientes bCli;
private Date hoy;
private Dia dias;
private AgendaTurnos agenda;
    @Override
    public boolean proceder() {
         boolean seguirCorriendo = true;
        boolean cerrar = false;
        String pas="";
        String usu="", nomProd="";
        float precio=0;
       
        int cant;  
   
        while (!cerrar) {
            int op;
            do {
                op = EntradaSalida.leerInt(
                        "MENÚ ADMINISTRADOR\n\n"
                        + "[0] Salir\n"
                        + "[1] Cerrar Sesión\n"
                        + "[2] Alta Recepcionista\n"
                        + "[3] Alta Veterinario\n"
                        + "[4] Alta Medicamento\n"
                        + "[5] Alta Producto Regular");
            } while (op < 0 || op > 5);
                 switch(op){
                     case 1:
                EntradaSalida.mostrarString("¡Hasta pronto!");
                         cerrar = true;
                         break;
                     case 0:     
                         cerrar = true;
                         seguirCorriendo = false;
                         break;
                     case 2:
                         do{
                            usu = EntradaSalida.leerString("Nombre de nuevo usuario: ");
                            if(personas.verificarUsuario(usu)){
                               EntradaSalida.mostrarString("Usuario ["+usu+" ya existe.");
                            }
                         }while(personas.verificarUsuario(usu));
                            pas = EntradaSalida.leerPassword("Contraseña del Nuevo usuario:");
                            altaRecepcionista(usu,pas);
                         break;
                     case 3:
                     
                     
                          do{
                            usu = EntradaSalida.leerString("Nombre de nuevo usuario: ");
                            if(personas.verificarUsuario(usu)){
                               EntradaSalida.mostrarString("Usuario ["+usu+"] ya existe..");
                               
                            }
                         }while(personas.verificarUsuario(usu));
                           
                            pas = EntradaSalida.leerPassword("Contraseña del Nuevo usuario:");
                            int[] dias={0,0,0,};
                            ArrayList<String> animales =new ArrayList<>();
                            animales.add(cargarAnimales());
                            
                            
                            while(EntradaSalida.leerBoolean("¿Atiende mas mascotas?")){
                            String aux=cargarAnimales();
                            boolean animalCargado=false;
                                 for(int j=0;j<animales.size();j++){
                                     if(animales.get(j).equals(aux)){
                                         EntradaSalida.mostrarString("Ya ingreso este tipo de mascota para el veterinario");
                                         animalCargado=true;
                                         break;
                                    }
                                  }
                                 if(!animalCargado){
                                     animales.add(aux);
                                 }
                             }
                          
                            for(int z=0;z<3;z++){
                            dias[z]=(cargarDias(dias[0],dias[1],dias[2]));
                            }
                            altaVeterinario(animales,dias,usu,pas);
                            
                         break;
                     case 4:
                         nomProd=EntradaSalida.leerString("Nombre del Producto: ");
                         precio=EntradaSalida.leerFloat("Precio del producto: ");
                         cant=EntradaSalida.leerInt("Cantidad:");
                         productos.altaMedicamento(nomProd,precio,cant);
                         break;
                     case 5:
                         nomProd=EntradaSalida.leerString("Nombre del producto: ");
                         precio=EntradaSalida.leerFloat("Precio: $");
                         cant=EntradaSalida.leerInt("Cantidad:");
                         productos.altaRegular(nomProd,precio,cant);
                         break;

            }
        }
        return seguirCorriendo;
    }

    public StockProducto getProductos() {
        return productos;
    }

    public void setProductos(StockProducto productos) {
        this.productos = productos;
    }

    public Personal getPersonas() {
        return personas;
    }

    public void setPersonas(Personal personas) {
        this.personas = personas;
    }

    private void altaRecepcionista(String usu, String pas) {
       Recepcionista r=new Recepcionista(usu,pas);
       r.setPro(productos);
       r.setbCli(bCli);
       r.setHoy(hoy);
       r.setDias(dias);
       r.setAgenda(agenda)
               
               ;
       personas.agregarPersona(r);
    }

    private void altaVeterinario(ArrayList<String> animales,int[] dia,String usu, String pas) {
      Veterinario v=new Veterinario(animales,dia,usu,pas);
      for (int i=0;i<3;i++){
          switch(dia[i]){
              case 1:
                 dias.agregarLunes(v);
                 break;
              case 2:
                 dias.agregarMartes(v);
                 break;
              case 3:
                 dias.agregarMiercoles(v);
                 break;
              case 4:
                 dias.agregarJueves(v);
                 break;
              case 5:
                 dias.agregarViernes(v);
                 break;
              case 6:
                 dias.agregarSabado(v);
                 break;
                  
          }
      }
      v.setPro(productos);
      
      personas.agregarPersona(v);
    }

    private int cargarDias(int a, int b, int c) {
       int d=3,i, dia=0;
      
       do{
           EntradaSalida.mostrarString("A continuacion debe elegir los dias["+d+"] laborables del veterinario");
           do{
           i=EntradaSalida.leerInt("[1]-Lunes\n"
                                 + "[2]-Martes\n"
                                 + "[3]-Miercoles\n"
                                 + "[4]-Jueves\n"
                                 + "[5]-Viernes\n"
                                 + "[6]-Sabado\n");
           }while(i<0||i>6);
                for(int j=0;j<3;j++){
                    if(i == a ||i == b || i == c){
                       EntradaSalida.mostrarString("Ya ingreso este dia para el veterinario"); 
                     
                       
                       break;
                    }else{
                   dia=i;
                   d--;
                    }      
                }
             
                
                
       }while(d>0);
       return dia;
    }
     private String cargarAnimales() {
       int i;
       String tipo="";
     
           EntradaSalida.mostrarString("Elija las mascotas que atiende el veterinario");
           do{
          
           i=EntradaSalida.leerInt("[1]-Perro\n"
                                 + "[2]-Gato\n"
                                 + "[3]-Tortuga\n"
                                 + "[4]-Canario");
           }while(i<1||i>4);
           switch(i){
               case 1:
                   tipo="PERRO";
                   break;
               case 2:
                   tipo="GATO";
                   break;
               case 3:
                   tipo="TORTUGA";
                   break;
               case 4:
                   tipo="CANARIO";
           }
                
        
       return tipo;
    }

    public void setHoy(Date hoy) {
        this.hoy = hoy;
    }

    public void setbCli(BaseClientes bCli) {
        this.bCli = bCli;
    }

    public Dia getDias() {
        return dias;
    }

    public void setDias(Dia dias) {
        this.dias = dias;
    }

    public AgendaTurnos getAgenda() {
        return agenda;
    }

    public void setAgenda(AgendaTurnos agenda) {
        this.agenda = agenda;
    }


}

