package veterinaria;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.Date;

public class Sistema implements Serializable{
    private AgendaTurnos turnos;
    private Personal personas;
    private StockProducto sPro;
    private Date hoy;
    
    
    private BaseClientes bCli;
    private Dia dias;
    public Sistema() {
        turnos = new AgendaTurnos();
        personas = new Personal();
        sPro=new StockProducto();
        hoy=new Date();
        hoy.setHours(0);
        hoy.setMinutes(0);
        dias=new Dia();
        bCli=new BaseClientes();
    }   
    public Sistema deSerializar(String archivo) throws IOException, ClassNotFoundException {
        FileInputStream f = new FileInputStream(archivo);
        ObjectInputStream o = new ObjectInputStream(f);
        Sistema s = (Sistema) o.readObject();
        return s;
    }

    public void serializar(String archivo) throws IOException {
        FileOutputStream f = new FileOutputStream(archivo);
        ObjectOutputStream o = new ObjectOutputStream(f);
        o.writeObject(this);
        o.close();
    }
     public void arrancar() {
        boolean corriendo = true;
     //  turnos.setDias(dias);
        while (corriendo) {
          

            String usuario = EntradaSalida.leerString("DIA: "+hoy.getDate()+"/"+(hoy.getMonth()+1)+"/"+(hoy.getYear()+1900)+"\nIngrese su usuario\n");
            String password = EntradaSalida.leerPassword("Ingrese password");
            Persona u=personas.buscar(usuario,password);
            

            if (u == null) {
                EntradaSalida.mostrarString("Usuario/contraseña inexistente");
            } else {
          
                corriendo = u.proceder();
            }
        }
        EntradaSalida.mostrarString("Hasta mañana\n"
                + "gracias por utilizar Sistema de CASTRO,HEBER");
    }

    public void inicializacion() {
        String usuario, password;

        EntradaSalida.mostrarString(
                "PRIMER ARRANQUE\n\n"
                + "A continuación genere administrador para el sistema."
        );

        usuario = EntradaSalida.leerString("Nombre de usuario del Administrador.");
        password = EntradaSalida.leerPassword("Contraseña del Administrador.");
        Administrador a = new Administrador(usuario, password);
        a.setHoy(hoy);
        a.setbCli(bCli);
        a.setPersonas(personas);
        a.setProductos(sPro);
        a.setDias(dias);
        a.setAgenda(turnos);
        personas.agregarPersona(a);
        EntradaSalida.mostrarString( "ADMINISTRADOR CARGADO\n\n"
                + "Ya puede ingresar al sistema."  );
    }

    public Date getHoy() {
        return hoy;
    }
  

    public AgendaTurnos getTurnos() {
        return turnos;
    }

    public void setTurnos(AgendaTurnos turnos) {
        this.turnos = turnos;
    }

    public Personal getPersonas() {
        return personas;
    }

    public void setPersonas(Personal personas) {
        this.personas = personas;
    }

    public StockProducto getsPro() {
        return sPro;
    }

    public void setsPro(StockProducto sPro) {
        this.sPro = sPro;
    }

    public BaseClientes getbCli() {
        return bCli;
    }

    public void setbCli(BaseClientes bCli) {
        this.bCli = bCli;
    }
    
}
